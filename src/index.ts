import * as fs from "fs";
import fetch from "node-fetch";
import { Client, TextChannel, RichEmbed, RichEmbedOptions } from "discord.js";

type WatchingChannels = { [string: string]: RichEmbedOptions; };

const client: Client = new Client();
const WATCH_CHANNELS: WatchingChannels = {
    "607987379701153826": {
        title: "Welcome to the stream!",
        description: "Don't forget to tune in on Twitch too!\n\nhttps://twitch.tv/luaq",
        url: "https://twitch.tv/luaq",
        author: {
            name: "luaq.#1337",
            url: "https://gitlab.com/Luaq/casual-bot-v2"
        }
    }
};

function sendCatFact() {
    // make sure another gets sent
    setTimeout(sendCatFact, conf.cat_fact.delay * 1000);

    fetch("https://catfact.ninja/fact").then(r => r.json()).then(json => {
        const fact = json.fact;
        const channel = client.channels.find(chan => chan.id === conf.cat_fact.channel);
        // validate that both the fact and channel exist
        if (!fact || !channel) {
            return;
        }
        // send the fact
        (<TextChannel> channel).send("", new RichEmbed({
            title: "Cat Fact",
            description: fact,
            footer: { text: "don't fact-check because fuck you" },
            color: 0x6BCFFF
        }));
    });
}

client.once("ready", () => {
    console.log(`Client ready as ${client.user.tag}`);

    // begin sending the cat facts
    sendCatFact();
});

client.on("voiceStateUpdate", (oldMember, newMember) => {
    const channelID = newMember.voiceChannelID;
    const embed = WATCH_CHANNELS[channelID];
    if (!embed || oldMember.voiceChannel) {
        return; // the channelID doesn't have an embed
    }
    // send a rich embed message based on which
    // voice channel was joined by member
    newMember.send(new RichEmbed(embed));
});

//
// read the config file
//

const conf_file: string = "files/config.json";

let conf: any = JSON.parse(fs.readFileSync(conf_file, "utf-8"));
if (!conf) {
    process.exit();
}

let bot_token: string | undefined = conf["token"];

if (bot_token) {
    // not using process.env this time
    client.login(bot_token).catch(() => {
        console.log("Login failed, please try updating the token.");
        conf["bot_token"] = undefined; // undefine the token
        fs.writeFileSync(conf_file, JSON.stringify(conf, null, 2)); // rewrite the file with the changes
        process.exit();
    });
}
